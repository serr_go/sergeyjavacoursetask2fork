package com.sergey.javacourse.task2.Parsers;

import com.sergey.javacourse.task2.CurrencyData.Currency;

import java.util.ArrayList;
import java.util.regex.*;

public class RegexNBRBParser extends Parser {

    /**
     * RegexNBRBParser
     * Makes HTML NBRB currency page parsing
     * using regex
     *
     * @param response
     * @return ArrayList of daily records.
     */
    @Override
    public ArrayList<Currency> parse(String response) {
        ArrayList<Currency> dailyRecord = new ArrayList<>();

        Pattern currencyPattern = Pattern.compile("titlecol\">"
                +"([A-Z]*?)</td><td>"
                +"([0-9]*)</td><td class=\"textcol\">"
                +"(.*?)</td><td>                        "
                +"([0-9,]*)");
        Matcher currMatch = currencyPattern.matcher(response);

        while(currMatch.find()) {
            double exchangeRate = Double.parseDouble(currMatch.group(4).replace(',','.'));
            dailyRecord.add(
                    new Currency(
                            currMatch.group(1),
                            Integer.parseInt(currMatch.group(2)),
                            currMatch.group(3),
                            exchangeRate));
        }

        return new ArrayList<>(dailyRecord);
    }
}