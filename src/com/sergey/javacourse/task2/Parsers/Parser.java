package com.sergey.javacourse.task2.Parsers;

import com.sergey.javacourse.task2.CurrencyData.Currency;

import java.util.ArrayList;

public abstract class Parser {
    public abstract ArrayList<Currency> parse(String response);
}
