package com.sergey.javacourse.task2;

import com.sergey.javacourse.task2.CurrencyData.CurrenciesStorage;
import com.sergey.javacourse.task2.Utils.RequestException;
import com.sergey.javacourse.task2.Utils.Requester;

import java.util.ArrayList;


public class ParallelRequester implements Runnable {
    private final String PAGE_URL =
            "http://www.nbrb.by/statistics/rates/ratesDaily.asp?date=";

    private int day = 0;
    private CurrenciesStorage storage;
    private ArrayList<String> responses = new ArrayList<>();
    private Requester requester = new Requester();

    public ParallelRequester(CurrenciesStorage storage) {
        this.storage = storage;
    }

    private synchronized String getDate() {
        return storage.getDaysList().get(day++);
    }

    private synchronized void addResponse(String response) {
        responses.add(response);
    }

    @Override
    public void run() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String date = getDate();

        try {
            addResponse(requester.sendGETRequest(PAGE_URL+date));
        } catch (RequestException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<String> getAllResponses() {
        return responses;
    }
}
